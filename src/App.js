import React from 'react';
import './App.css';

function generateDays(n, startDateGen) {
    let res = [];
    for(let i=0; i < n; i++) {
        let d = startDateGen();
        d.setDate(d.getDate() + i);
        res.push(d);
    }
    return res;
}

const border = "1px solid black";

    const cellStyle = (d, lineNbr) => ({
        borderBottom: border,
        borderRight: border,
        fontSize: "10px",
        height: "32px",
        background: (d.getDay() === 0 || d.getDay() === 6 ? "#ddd" : (lineNbr % 2 === 0) ? "#fff" : "#eee"),
        boxSizing: "border-box",
        padding: "2px 4px",
        display: "flex",
        flexFlow: "column",
        alignItems: "flex-end",
        justifyContent: "flex-end",
    });

    const headerStyle = (d) => ({
        ...cellStyle(d),
        borderTop: border,
        display: "flex",
        flexFlow: "column",
        alignItems: "center",
        justifyContent: "center",
    });

    const leftCellStyle = (lineNbr) => ({
        borderBottom: border,
        borderLeft: border,
        borderRight: border,
        background: (lineNbr % 2 === 0) ? "#fff" : "#eee",
        height: "32px",
        textAlign: "left",
        padding: "2px 6px",
        boxSizing: "border-box"
    });

    const leftHeaderCellStyle = () => ({
        ...leftCellStyle(),
        borderTop: border
    });

function HabitLine2(props) {

    return (
        <>
            <div style={leftCellStyle(props.lineNbr)}>{props.habit.title}</div>
            {props.days.map((d,i) => <div style={cellStyle(d,props.lineNbr)}>{props.habit.texts ? props.habit.texts[i] : ""}</div>)}
        </>
    )
}

function HabitTable2(props) {
    return (
        <div style={{display: 'grid', gridTemplateColumns: "300px repeat(28,35px)", marginTop: "100px" }}>
            <div style={leftHeaderCellStyle()}></div>
            {props.days.map(d => <div style={headerStyle(d)}>{d.getMonth()+1 + "/" + d.getDate()}</div>)}

            {props.habits.map((habit, idx) => <HabitLine2 habit={habit} days={props.days} lineNbr={idx} />)}
        </div>
    )
}

function createPlus2EveryOtherDayGenerator(start) {
    return (dayN) => Math.floor(start + dayN / 2);
}

function createIncEveryThirdDayGenerator(start) {
    return (dayN) => Math.floor(start + dayN / 3);
}

function createXEveryOtherDayGenerator() {
    return (dayN) => dayN % 2 ? 'x' : '';
}

const createJulHabits = (days) => {

    return [
        {title: "Wake up before 6.30am"},
        {title: "Drink water 0.5"},
        {title: "Walk 15m"},
        {title: "Cold shower" },
        {title: "Breakfast"},
        {title: "Vitamin C"},
        {title: "Work on PhD thesis (1h)"},
        {title: "Gym"},
        {title: "Gym Serious"},
        ...new Array(2).fill("").map(_ => ({title: ""})),
        {title: "Lunch" },
        {title: "Meditation 5m"},
        {title: "Journal"},
        {title: "Call parents or gran or sister"},
        ...new Array(2).fill("").map(_ => ({title: ""})),
        {title: "Dinner" },
        {title: "Read/listen to a book (40m)", texts: days.map((e,i) => createXEveryOtherDayGenerator()(i))},
        {title: "Read a professional magazine", texts: days.map((e,i) => createXEveryOtherDayGenerator()(i+1))},
        {title: "Housework (30m)"},
        {title: "Cosmetology"},
        {title: "Plan tomorrow"},
        ...new Array(1).fill("").map(_ => ({title: ""})),
    ];
}

const createMyHabits = (days) => {

    return [
        {title: "Wake up by 7.00am"},
        {title: "Drink water 0.5"},
        {title: "Jumping jacks, 30"},
        {title: "Cold shower, 1m" },
        {title: "Morning piano 15m" },
        {title: "Vitamin C"},
        {title: "Start working by 9.15"},
        {title: "Skllled morning 1h"},
        {title: "Clocked 4h of work"},
        {title: "Clocked 8h of work"},
        {title: "Gym"},
        {title: "Meditation (mindfulness) 5m"},
        {title: "Call gran / mom"},
        {title: "Walk"},
        {title: "Read book 30m"},
        {title: "Plan day evening"},
        { title: "Teeth: monothing"},
        { title: "Teeth: floss"},
        { title: "Teeth: evening"},
        {title: "Journal"},
        {title: "Gen Art"},
        {title: "Piano practice" },
        { title: "Checkin"},
        {title: "Go to bed before 1am"},
        { title: ""},
        { title: ""},
    ];
}

function App() {

    let days = generateDays(28, () => new Date(2021,1,15));
    return (
        <div className="App">
            <HabitTable2 habits={createMyHabits(days)} days={days} />
            <HabitTable2 habits={createJulHabits(days)} days={days} />
        </div>
        );
}

export default App;
